#include "MuonJiveXML/CSCClusterRetriever.h"
#include "MuonJiveXML/TgcPrepDataRetriever.h"
#include "MuonJiveXML/CscPrepDataRetriever.h"
#include "MuonJiveXML/MdtPrepDataRetriever.h"
#include "MuonJiveXML/RpcPrepDataRetriever.h"
//#include "MuonJiveXML/MuidTrackRetriever.h"
#include "MuonJiveXML/TrigRpcDataRetriever.h"
#include "MuonJiveXML/TrigMuonROIRetriever.h"
#include "MuonJiveXML/sTgcPrepDataRetriever.h"
#include "MuonJiveXML/MMPrepDataRetriever.h"

using namespace JiveXML;

DECLARE_COMPONENT( CSCClusterRetriever )
DECLARE_COMPONENT( TgcPrepDataRetriever )
DECLARE_COMPONENT( CscPrepDataRetriever )
DECLARE_COMPONENT( MdtPrepDataRetriever )
DECLARE_COMPONENT( RpcPrepDataRetriever )
//DECLARE_COMPONENT( MuidTrackRetriever )
DECLARE_COMPONENT( TrigRpcDataRetriever )
DECLARE_COMPONENT( TrigMuonROIRetriever )
DECLARE_COMPONENT( sTgcPrepDataRetriever )
DECLARE_COMPONENT( MMPrepDataRetriever )

